import helpers.MyHelper;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;



public abstract class BaseTest {
    protected EventFiringWebDriver driver = null;
    protected MyHelper myHelper;


//region webDriverFactory
    protected void webDriverFactory(Browsers chosenOneBrowser) {
        switch (chosenOneBrowser) {
            case CHROME -> startChromeDriver();
            case FIREFOX -> startFFoxDriver();
            case OPERA -> startOperaDriver();
            default -> throw new IllegalArgumentException("Unknown driver" + chosenOneBrowser);
        }
    }


    private void startFFoxDriver() {
        WebDriverManager.firefoxdriver().setup();
        driver= new EventFiringWebDriver(new FirefoxDriver());
        driver.register(new HighlightListener());
    }

    private void startChromeDriver() {
        WebDriverManager.chromedriver().setup();
        driver = new EventFiringWebDriver(new ChromeDriver());
        driver.register(new HighlightListener());
    }

    private void startOperaDriver() {
        WebDriverManager.operadriver().setup();
        driver = new EventFiringWebDriver(new OperaDriver());
        driver.register(new HighlightListener());
    }
//endregion webDriverFactory
// region helpers
protected void initHelpers() {

    myHelper = new MyHelper(driver);
}

    protected MyHelper withMyHelper() {
        return myHelper;
    }
    // endregion helpers

    @BeforeMethod
    void setUp() {
        webDriverFactory(Browsers.CHROME);
    }

    @AfterMethod
    void tearDown() { if (driver != null)
        driver.quit();
    }
}