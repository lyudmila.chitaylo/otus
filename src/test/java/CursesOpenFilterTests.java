import org.testng.annotations.Test;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.stringContainsInOrder;

public class CursesOpenFilterTests extends BaseTest {

    @Test
    void cursesOpenTest()  {
        initHelpers();
        withMyHelper().goOtus();
        String expectedPageTitle = "����������������";
        withMyHelper()
                //.stickyClose() *���� � ������ �� ��� ������ �� ��������
                .exeptCook()
                .openCurses();
        String title = withMyHelper()
                .getPageMainText("DevCursesPage");
        assertThat("It should be Developers curses page", title, stringContainsInOrder(expectedPageTitle));
    }

    @Test
    void filterCursesByAbc() {
        initHelpers();
        withMyHelper().goOtus()
                //.stickyClose() *���� � ������ �� ��� ������ �� ��������
                .exeptCook()
                .openCurses();
        List<String> names = withMyHelper().getCursesNames();
        System.out.printf("Not filtered list of names: %s%n",names);
        List<String> abcNames = names.stream().sorted().collect(Collectors.toList());
        System.out.printf("Filtered list of names: %s%n",abcNames);
    }

    @Test
    void filterDatesFindMinMax() {
        initHelpers();
        withMyHelper().goOtus()
                //.stickyClose() *���� � ������ �� ��� ������ �� ��������
                .exeptCook()
                .openCurses();
        List<LocalDate> dates = withMyHelper().getCursesStartDates();
        System.out.printf("Not filtered list of dates: %s%n", dates);
        LocalDate closestDate= dates.stream().min(LocalDate::compareTo).get();
        System.out.printf("Closest curse starts at: %s%n",closestDate);
        LocalDate latestDate= dates.stream().max(LocalDate::compareTo).get();
        System.out.printf("Latest curse starts at: %s%n",latestDate);

    }
}
