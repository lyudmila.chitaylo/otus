package helpers;

import Otus.Page.DevCursesPage;
import Otus.Page.OtusMainPage;
import lombok.Getter;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class MyHelper {
    private EventFiringWebDriver driver;
    private WebDriverWait wait;

    @Getter
    protected static final String otusURL = "https://otus.ru";

    public MyHelper(EventFiringWebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver,
                TimeUnit.SECONDS.toSeconds(5),
                TimeUnit.SECONDS.toMillis(1));


    }
    // region page initialization

    protected OtusMainPage onOtusMainPage() {
        return new OtusMainPage(driver);
    }

    protected DevCursesPage onDevCursesPage() {
        return new DevCursesPage(driver);
    }


    // endregion page initialization


    public MyHelper goOtus() {
        driver.manage().window().maximize();
        driver.get(otusURL);
        return this;
    }

    public MyHelper exeptCook() {
        onOtusMainPage().getButtonCookiesOK().click();
        return this;
    }

    public MyHelper stickyClose() {
        onOtusMainPage().getStickyCross().click();
        return this;
    }


    public MyHelper openCurses() {
        Actions actions = new Actions(driver);
        WebElement moreCursesButton = onOtusMainPage().getButtonMoreCurse();
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", moreCursesButton);
        actions.click(moreCursesButton).perform();
        return this;
    }

    public String getPageMainText(String pageName) {
        switch (pageName) {
            case ("DevCursesPage"):
                return onDevCursesPage().getPageTile().getText();
            case ("OtusMainPage"):
                return onOtusMainPage().getPageTile().getText();
            default:
                System.out.println("No such Page!");
        }

        return pageName;
    }

    public List<String> getCursesNames() {
        List<WebElement> curses = onDevCursesPage().getCursesTitles();
        List<String> names = new ArrayList<>();
        for (WebElement curs : curses) {
            String text = curs.getText();
            names.add(text);
        }
        return names;

    }

    public List<LocalDate> getCursesStartDates() {
        List<WebElement> startsElements = onDevCursesPage().getCursesStarts();
        List<LocalDate> starts = new ArrayList<>();
        startsElements.stream().filter(e -> e.getText()
                .matches(".*\\d.*")).forEach(webElement ->
                starts.add(dateConverted(webElement.getText())));


        return starts;
    }


    private static LocalDate dateConverted(String stringDate) {
        //from d MMMM yyyy -> dd.MM.yyyy
        String toBeConverted = stringDate.substring(2).concat(" 2021");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d MMMM yyyy").withLocale(Locale.forLanguageTag("ru"));
        if (stringDate.matches(".*2022.*")) toBeConverted = stringDate.substring(2, 16);

        return LocalDate.parse(toBeConverted, formatter);
    }


}
