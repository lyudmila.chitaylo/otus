package Otus.Page;

import lombok.Getter;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import java.util.List;

@Getter
public class DevCursesPage extends AbstractPage{
    public DevCursesPage(EventFiringWebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(xpath =  "//h1")
    private WebElement pageTile;

    @FindBy(xpath = "//div[@class='lessons__new-item-title lessons__new-item-title_with-bg js-ellipse']")
    private List<WebElement> cursesTitles;

    @FindBy(xpath = "//div[@class='lessons__new-item-start']")
    private List<WebElement> cursesStarts;

}
