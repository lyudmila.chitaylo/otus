package Otus.Page;

import lombok.Getter;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.events.EventFiringWebDriver;


@Getter
public class OtusMainPage extends AbstractPage {

    public OtusMainPage(EventFiringWebDriver webDriver) {
        super(webDriver);
    }

    //region Elements

    @FindBy(xpath = "//div[@class='button button_white button_white-inverse-transparent button_md-3 button_radius-md ']")
    private WebElement buttonCurse;

    @FindBy(xpath = "//*[@class='button button_blue transitional-main__courses-more']")
    private WebElement buttonMoreCurse;

    @FindBy(xpath = "//button[@class='js-cookie-accept cookies__button']")
    private WebElement buttonCookiesOK;

    @FindBy(xpath = "//div[@class='sticky-banner__close js-sticky-banner-close']")
    private WebElement stickyCross;


    @FindBy(xpath = "//a[@class='nav__item course-categories__nav-item_active course-categories__nav-item']")
    private WebElement devCurse;

    @FindBy(xpath = "//h1")
    private WebElement pageTile;

    //endregion Elements
}
