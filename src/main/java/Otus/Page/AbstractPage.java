package Otus.Page;

import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.EventFiringWebDriver;

public abstract class AbstractPage {
    public AbstractPage(final EventFiringWebDriver webDriver) {

        PageFactory.initElements(webDriver, this);
    }


}
